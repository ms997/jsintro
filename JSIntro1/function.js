//let's make some lavendar ice cream with functions
//measurements in cups, tbsp, tsp and oz
const recipe = {
    ingredients:
    {
        heavyCream: 3,
        lavender: 3,
        sweetenedCondensedMilk: 14,
        salt: 1/8,
        vanillaExtract: 1,
        purpleGelFoodColoring: 1
    },
    steps: ['Freeze the cake pan,', 'Cook half of the heavy cream with lavendar,', 'Cool the mixture and whip till firm peak,', 'Mix the rest of the ingredients,', 'Freeze again!'],
    serving: 4
};
let pantry = {
  heavyCream: 2,
  sweetenedCondensedMilk: 14,
  salt: 100,
  vanillaExtract: 5,
  iceCreamServing: 0
};

/* Check if there are lavendar ice cream in the pantry!
In this exercise, using functions in addition to if else statements and loops to make lavendar ice cream!
Follow the sudo code!
*/

/*
    1. Try to eat all the lavendar ice cream
    hint: you need a function to eat lavendar ice cream

    2. Make more lavendar ice cream!
    hint: you need a function to make lavendar ice cream, checkPantry, getMoreIngredients
    hint 2: after you get more ingredients, you need to try to make lavendar ice cream again!
*/

//BONUS: eat a certain servings of lavendar ice cream
//make sure you think of edge cases! What if you want to eat more lavendar ice cream than the servings of lavendar ice cream? Or if you don't want to eat any lavendar ice cream because you want to save them for your friends?
